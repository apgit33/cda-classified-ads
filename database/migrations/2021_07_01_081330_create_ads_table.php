<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->id()->comment('ID');
            $table->string('title')->comment('Title');
            $table->integer('price')->nullable()->comment('Price');
            $table->text('description')->comment('Description');
            $table->text('picture')->nullable()->comment('Picture');
            $table->string('status')->default('pending')->comment('Status');
            $table->text('city')->comment('City');
            $table->foreignId('user_id')->constrained()->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {    
        Schema::dropIfExists('ads');
    }
}
