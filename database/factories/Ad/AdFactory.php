<?php

namespace Database\Factories\Ad;

use App\Models\Ad\Ad;
use App\Models\Ad\Category;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class AdFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Ad::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        if (User::count() == 0) {
            User::factory(10)->create();
        }
        
        return [
            'title' => $this->faker->sentence(4),
            'price' => $this->faker->randomNumber(4),
            'description' => $this->faker->sentence(30),
            'status' => $this->faker->randomElement(['pending','validated','canceled']),
            'city' => $this->faker->word(),
            'user_id' => $this->faker->randomElement(User::all()->pluck('id')->toArray()),
            'picture' => null,
            'is_viewable' => false,
        ];
    }
}
