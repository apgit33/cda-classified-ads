<?php

namespace Database\Seeders;

use App\Models\Ad\Ad;
use App\Models\Ad\Category as AdCategory;
use App\Models\Ad\Image;
use App\Models\User\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(10)->create(); // populate User
        AdCategory::factory(10)->create(); // populate Category
        Ad::factory(80)->create(); // populate Ad

        foreach (Ad::all() as $ad) { // attach categories to ad
            $ad->categories()->attach(array_rand(AdCategory::all()->pluck('id')->toArray(), rand(1,3)));
        }
    }
}
