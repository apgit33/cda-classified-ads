@setup
    $release = date('YmdHis');
    $new_release_dir = $releasesdir .'/'. $release;
@endsetup

@servers(['web' => $user.'@'.$host,'localhost' => '127.0.0.1'])

@story('deploy', ['on' => 'web'])
    clone_repository
    run_composer
    copy
    init_project
    init_database
    opti
    update_symlinks
    clean_old_releases
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releasesdir }} ] || mkdir {{ $releasesdir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}
    composer update
    composer install --prefer-dist --no-scripts -q -o
@endtask

@task('copy')
    echo "Generating .env"

    echo "Copy .env"
    cp {{ $new_release_dir }}/.env.example {{ $new_release_dir }}/.env
    
    echo "Writing variable in .env"
    echo "DB_HOST="{{ $dbhost }} >> {{ $new_release_dir }}/.env
    echo "DB_USERNAME={{ $user }}" >> {{ $new_release_dir }}/.env
    echo "DB_DATABASE={{ $dbname }}" >> {{ $new_release_dir }}/.env
    echo "DB_PASSWORD=\"{{ $dbpassword }}\"" >> {{ $new_release_dir }}/.env
    echo "APP_URL=\"https://{{ $user }}.{{ $host }}\"" >> {{ $new_release_dir }}/.env
    echo "APP_CLIENT_ID=\"{{ $client }}\"" >> {{ $new_release_dir }}/.env
    echo "L5_SWAGGER_CONST_HOST=\"https://{{ $user }}.{{ $host }}{{ $apidir }}\"" >> {{ $new_release_dir }}/.env
    echo "API_DOMAIN=\"https://{{ $user }}.{{ $host }}\"" >> {{ $new_release_dir }}/.env

@endtask

@task('init_project')
    cd {{ $new_release_dir }}
    
    echo "Generate key"
    php artisan key:generate

    @if ($firstdeploy == 1)
        echo "First initialisation"
        php artisan optimize:clear

        echo "Generate passport"
        php artisan passport:install

        echo "Generate passport keys"
        php artisan passport:keys --force
    @endif
@endtask

@task('init_database')
    cd {{ $new_release_dir }}
    
    echo "Create database if not exist"
    php artisan make:database

    echo "Run migration"
    php artisan migrate --force
@endtask

@task('opti')
    @if ($firstdeploy == 1)
        echo "APP_DEBUG=false" >> {{ $new_release_dir }}/.env

        echo "Copy .env and storage"
        cp {{ $new_release_dir }}/.env {{ $appdir }}/.env
        
        mkdir -p {{ $appdir }}/storage
        
        sudo -n chown -R {{ $user }} {{ $appdir }}/storage
        
        cp -r {{ $new_release_dir }}/storage {{ $appdir }}

        sudo -n chown -R www-data {{ $appdir }}/storage

    @endif

    cd {{ $new_release_dir }}

    echo "Optimization"
    php artisan config:cache

    php artisan route:cache
@endtask

@task('update_symlinks')
    rm -rf {{ $new_release_dir }}/storage
    rm -f {{ $new_release_dir }}/.env

    echo 'Linking .env file'
    ln -nfs {{ $appdir }}/.env {{ $new_release_dir }}/.env
    
    echo "Linking storage directory"
    ln -nfs {{ $appdir }}/storage {{ $new_release_dir }}/storage

    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $appdir }}/current
@endtask

@task('clean_old_releases')
    echo "Cleaning old releases"
    cd {{ $releasesdir }}
    ls -dt {{ $releasesdir }}/* | tail -n +3 | xargs -d "\n" rm -rf;
@endtask