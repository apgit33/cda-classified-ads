<?php

namespace Tests\Model;

use App\Models\Ad\Ad;
use App\Models\Ad\Category;
use App\Models\User\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * Tests in this class :
 * ✓ create ten ads
 * ✓ can delete ad
 * ✓ can update ad
 * ✓ create ad with categories
 */
class AdTest extends TestCase
{
    use RefreshDatabase;

   /**
     * Test create categories
     *
     * @return void
     */
    public function test_createTenAds()
    {
        $count = Ad::all()->count();

        Ad::factory(10)->create();

        $this->assertDatabaseCount('ads', $count + 10);
    }

    /**
     * Test delete categories
     *
     * @return void
     */
    public function test_canDeleteAd()
    {
        $ad = Ad::factory()->create();

        $ad->delete();

        $this->assertDeleted($ad);
    }

    /**
     * Test update categories
     *
     * @return void
     */
    public function test_canUpdateAd()
    {
        $ad = Ad::factory()->state([
            'title' => 'test',
        ])->create();

        $ad->update([
            'title' => 'new name'
        ]);

        $this->assertEquals('new name', $ad->title);
    }

    /**
     * Test create ad with categories
     *
     * @return void
     */
    public function test_createAdWithCategories()
    {
        $categories = Category::factory()->count(3)->create();

        $ad = Ad::factory()
            ->hasAttached($categories)
            ->create();

        $this->assertDatabaseHas('ad_category', [
            'ad_id' => $ad->id,
        ]);  
    }
}
