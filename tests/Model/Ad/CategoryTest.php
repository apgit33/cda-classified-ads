<?php

namespace Tests\Model;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use App\Models\Ad\Category;

/**
 * Tests in this class :
 * ✓ can create ten categories
 * ✓ can delete category
 * ✓ can update category
 */
class CategoryTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test create categories
     *
     * @return void
     */
    public function test_canCreateTenCategories()
    {
        $count = Category::all()->count();

        Category::factory(10)->create();

        $this->assertDatabaseCount('categories', $count + 10);
    }

    /**
     * Test delete categories
     *
     * @return void
     */
    public function test_canDeleteCategory()
    {
        $category = Category::factory()->create();

        $category->delete();

        $this->assertDeleted($category);
    }

    /**
     * Test update categories
     *
     * @return void
     */
    public function test_canUpdateCategory()
    {
        $category = Category::factory()->state([
            'name' => 'test',
        ])->create();

        $category->update([
            'name' => 'new name'
        ]);

        $this->assertEquals('new name', $category->name);
    }
}
