<?php

namespace Tests\Model\User;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use App\Models\Ad\Ad;
use App\Models\User\User;

/**
 * Tests in this class :
 * ✓ create ten users
 * ✓ create admin user
 * ✓ delete user
 * ✓ update user
 * ✓ create user with ad
 */
class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test create regular user
     *
     * @return void
     */
    public function test_createTenUsers()
    {
        $count = User::all()->count();

        User::factory(10)->create();

        $this->assertDatabaseCount('users', $count + 10);
    }

    /**
     * Test create admin user
     *
     * @return void
     */
    public function test_createAdminUser()
    {
        User::factory(10)->create();
        User::factory(1)->admin()->create();

        $this->assertDatabaseHas('users', [
            'is_admin' => true,
        ]);   
    }

    /**
     * Test delete admin
     *
     * @return void
     */
    public function test_deleteUser()
    {
        $user = User::factory()->create();

        $user->delete();

        $this->assertDeleted($user);
    }

    /**
     * Test update user
     *
     * @return void
     */
    public function test_updateUser()
    {
        $user = User::factory()->state([
            'name' => 'Abigail Otwell',
        ])->create();

        $user->update([
            'name' => 'new name'
        ]);

        $this->assertEquals('new name', $user->name);
    }

    /**
     * Test create user with ads
     *
     * @return void
     */
    public function test_createUserWithAd()
    {
        $user = User::factory()
            ->hasAds(3)
            ->create();

        $ads = Ad::whereUserId($user->id)->count();

        $this->assertEquals($ads, 3);
    }
}
