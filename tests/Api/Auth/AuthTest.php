<?php

namespace Tests\Api\Auth;

use Laravel\Passport\Passport;
use Tests\TestCase;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;


use App\Models\User\User;

/**
 * Tests in this class :
 * ✓ unauth can ask for password valid email
 * ✓ unauth can ask for password invalid email
 * ✓ unauth can ask for password invalid emaild
 * ✓ unauth can reset password
 * ✓ unauth can not reset password wrong token
 * ✓ unauth can not reset password wrong parameter
 * ✓ unauth can not logout
 * ✓ auth can create token login and logout
 */
class AuthTest extends TestCase
{
    use RefreshDatabase;

    /**
     * ====================
     * ==> TEST FORGOT PASSWORD
     * ====================
     */

    /**
     * Test unauth asking for password (existing email) - 200
     *
     * @return void
     */
    public function test_unauthCanAskForPasswordValidEmail()
    {
        $user = User::factory()->create();

        $response = $this->post('/api/forgot-password', [
            'email' => $user->email,
        ]);

        $response->assertStatus(200);
    }

    /**
     * Test unauth asking for password (no existing email) - 200
     *
     * @return void
     */
    public function test_unauthCanAskForPasswordInvalidEmail()
    {
        $response = $this->post('/api/forgot-password', [
            'email' => 'test@test.com',
        ]);

        $response->assertStatus(200);
    }

    /**
     * Test unauth asking for password wrong parameter - 422
     *
     * @return void
     */
    public function test_unauthCanAskForPasswordInvalidEmaild()
    {
        $response = $this->post('/api/forgot-password', [
            'email' => '',
        ]);

        $response->assertStatus(422);
    }
    /**
     * ====================
     * ==> TEST RESET PASSWORD
     * ====================
     */

    /**
     * Test unauth reset his password - 200
     *
     * @return void
     */
    public function test_unauthCanResetPassword()
    {
        $user = User::factory()->state(['email'=>'test@test.com', 'password' => 'Old password'])->create();

        $token = Password::createToken($user);

        $response = $this->post('/api/reset-password/'.$token, [
            'email' => $user->email,
            'password' => 'test'
        ]);

        $user = User::whereEmail($user->email)->first();

        $this->assertTrue(Hash::check('test', $user->password));
    }

    /**
     * Test unauth reset his password wrong token - 200
     *
     * @return void
     */
    public function test_unauthCanNotResetPasswordWrongToken()
    {
        $user = User::factory()->state(['email'=>'test@test.com', 'password' => 'No hashed'])->create();

        $token = Password::createToken($user);

        $response = $this->post('/api/reset-password/'.$token.'test', [
            'email' => $user->email,
            'password' => 'new password'
        ]);

        $user = User::whereEmail($user->email)->first();

        $response->assertStatus(200);

        $this->assertSame('No hashed', $user->password);
    }

    /**
     * Test unauth reset his password wrong parameter - 422
     *
     * @return void
     */
    public function test_unauthCanNotResetPasswordWrongParameter()
    {
        $user = User::factory()->state(['email'=>'test@test.com'])->create();

        $token = Password::createToken($user);

        $response = $this->post('/api/reset-password/'.$token, [
            'email' => $user->email,
            'password' => ''
        ]);

        $user = User::whereEmail($user->email)->first();

        $response->assertStatus(422);
    }

    /**
     * ====================
     * ==> TEST Logout
     * ====================
     */

    /**
    * Test unauth logout - 401
    *
    * @return void
    */
    public function test_unauthCanNotLogout()
    {
        $response = $this->post('/api/logout');

        $response->assertUnauthorized();
    }


    /**
     * Test auth token login and logout - 200
     *
     * @return void
     */
    public function test_authCanCreateTokenLoginAndLogout()
    {
        $user = User::factory()->create();
        
        Passport::actingAs($user);

        $accessToken = $user->createToken('test')->accessToken;

        $response = $this->post('/api/logout');

        $token = Auth::user()->tokens;

        $this->assertNotNull($accessToken);

        $response->assertOk();
    }
}
