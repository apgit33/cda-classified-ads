<?php

namespace Tests\Api\User;

use Laravel\Passport\Passport;
use Tests\TestCase;

use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\Ad\Ad;
use App\Models\User\User;

/**
 * Tests in this class :
 * ✓ unauth can register
 * ✓ unauth can not create missing parameter
 * ✓ unauth can not create wrong parameter
 * ✓ unauth can not create same user
 * ✓ unauth can access users
 * ✓ unauth can not access users wrong parameters
 * ✓ auth can not access user empty
 * ✓ unauth can not access unknow user
 * ✓ admin can update other user
 * ✓ admin can not update unknow user
 * ✓ admin can not update wrong parameters
 * ✓ admin can not update wrong user
 * ✓ unauth can not update
 * ✓ auth can not update other
 * ✓ auth can update himself and only available parameter
 * ✓ admin can delete foreign user
 * ✓ unauth can not delete
 * ✓ admin can not delete unknow
 * ✓ admin can not delete wrong
 * ✓ auth can not delete foreign user
 * ✓ auth can delete himself
 */
class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * ====================
     * ==> TEST POST
     * ====================
     */

    /**
     * Test unauth register - 201
     *
     * @return void
     */
    public function test_unauthCanRegister()
    {
        $response = $this->post('/api/users', [
            'name' => 'test',
            'email' => 'test@test.com',
            'password' => 'test',
            'password_confirmation' => 'test',
        ]);

        $response->assertCreated();
    }

    /**
     * Test unauth register missing parameters - 422
     *
     * @return void
     */
    public function test_unauthCanNotCreateMissingParameter()
    {
        $response = $this->post('/api/users', [
            //
        ]);

        $response->assertStatus(422);
    }

    /**
     * Test unauth register wrong parameters - 422
     *
     * @return void
     */
    public function test_unauthCanNotCreateWrongParameter()
    {
        $response = $this->post('/api/users', [
            'name' => '°°°',
            'email' => 'test@test.com',
            'password' => 'test',
            'password_confirmation' => 'test',
        ]);

        $response->assertStatus(422);
    }

    /**
     * Test unauth register same user - 422
     *
     * @return void
     */
    public function test_unauthCanNotCreateSameUser()
    {
        $this->post('/api/users', [
            'name' => 'test',
            'email' => 'test@test.com',
            'password' => 'test',
            'password_confirmation' => 'test',
        ]);

        $response = $this->post('/api/users', [
            'name' => 'test',
            'email' => 'test@test.com',
            'password' => 'test',
            'password_confirmation' => 'test',
        ]);

        $response->assertStatus(422);
    }

    /**
     * ====================
     * ==> TEST GET all
     * ====================
     */

    /**
     * Test unauth access all users - 200
     *
     * @return void
     */
    public function test_unauthCanAccessUsers()
    {
        $response = $this->get('/api/users');

        $response->assertOk();
    }

    /**
     * Test unauth access all users wrong - 422
     *
     * @return void
     */
    public function test_unauthCanNotAccessUsersWrongParameters()
    {
        $response = $this->get('/api/users?name=°°°');

        $response->assertStatus(422);
    }

    /**
     * ====================
     * ==> TEST GET {id}
     * ====================
     */

    /**
     * Test unauth access user - 200
     *
     * @return void
     */
    public function test_authCanNotAccessUserEmpty()
    {
        $user = User::factory()->create();

        $response = $this->get('/api/users/'.$user->id);

        $response->assertOk();
    }

    /**
     * Test unauth access unknow user - 404
     *
     * @return void
     */
    public function test_unauthCanNotAccessUnknowUser()
    {
        $user = User::factory()->create();

        $response = $this->get('/api/users/test');

        $response->assertNotFound();
    }

    /**
     * ====================
     * ==> TEST PUT
     * ====================
     */

    /**
     * Test admin update users - 200
     *
     * @return void
     */
    public function test_adminCanUpdateOtherUser()
    {
        Passport::actingAs(User::factory()->admin()->create());

        $user = User::factory()->create();

        $response = $this->put('/api/users/'.$user->id, [
            'name' => 'John',
            'is_admin' => true,
        ]);

        $user = User::findOrFail($user->id);

        $response->assertOk();

        $this->assertSame('John', $user->name);
        
        $this->assertTrue($user->is_admin);
    }

    /**
     * Test admin update unknow user - 404
     *
     * @return void
     */
    public function test_adminCanNotUpdateUnknowUser()
    {
        Passport::actingAs(User::factory()->admin()->create());

        $response = $this->put('/api/users/1', [
            'name' => 'John',
        ]);

        $response->assertNotFound();
    }

    /**
     * Test admin update users wrong - 422
     *
     * @return void
     */
    public function test_adminCanNotUpdateWrongParameters()
    {
        Passport::actingAs(User::factory()->admin()->create());

        $user = User::factory()->create();

        $response = $this->put('/api/users/'.$user->id, [
            'name' => '°°°',
        ]);

        $response->assertStatus(422);
    }

    /**
     * Test admin update wrong user - 500
     *
     * @return void
     */
    public function test_adminCanNotUpdateWrongUser()
    {
        Passport::actingAs(User::factory()->admin()->create());

        $response = $this->put('/api/users/°°°', [
            'name' => 'John',
        ]);

        $response->assertStatus(500);
    }

    /**
     * Test unauth update users - 401
     *
     * @return void
     */
    public function test_unauthCanNotUpdate()
    {
        $response = $this->put('/api/users/1', [
            'name' => 'John',
        ]);

        $response->assertUnauthorized();
    }

    /**
     * Test auth update other users - 403
     *
     * @return void
     */
    public function test_authCanNotUpdateOther()
    {
        $user = User::factory()->create();

        $foreignUser = User::factory()->create();
        
        Passport::actingAs($user);

        $response = $this->put('/api/users/'.$foreignUser->id, [
            'name' => 'John',
        ]);

        $response->assertForbidden();
    }

    /**
     * Test auth update himself - 200
     *
     * @return void
     */
    public function test_authCanUpdateHimselfAndOnlyAvailableParameter()
    {
        $user = User::factory()->create();

        Passport::actingAs($user);
        
        $response = $this->put('/api/users/'.$user->id, [
            'name' => 'John',
            'is_admin' => true,
        ]);

        $user = User::findOrFail($user->id);

        $response->assertOk();

        $this->assertSame('John', $user->name);

        $this->assertFalse($user->is_admin);
    }

    /**
     * ====================
     * ==> TEST DELETE
     * ====================
     */

    /**
     * Test admin delete users - 204
     *
     * @return void
     */
    public function test_adminCanDeleteForeignUser()
    {
        Passport::actingAs(User::factory()->admin()->create());
        
        $user = User::factory()->create();

        $response = $this->delete('/api/users/'.$user->id, );

        $user = User::find($user->id);

        $response->assertNoContent();

        $this->assertNull($user);
    }

    /**
     * Test unauth delete user - 401
     *
     * @return void
     */
    public function test_unauthCanNotDelete()
    {
        $response = $this->delete('/api/users/1');

        $response->assertUnauthorized();
    }

    /**
     * Test admin delete unknow user - 404
     *
     * @return void
     */
    public function test_adminCanNotDeleteUnknow()
    {
        Passport::actingAs(User::factory()->admin()->create());

        $response = $this->delete('/api/users/433');

        $response->assertNotFound();
    }

    /**
     * Test admin delete wrong user - 500
     *
     * @return void
     */
    public function test_adminCanNotDeleteWrong()
    {
        Passport::actingAs(User::factory()->admin()->create());

        $response = $this->delete('/api/users/test');

        $response->assertStatus(500);
    }

    /**
     * Test auth delete other user - 403
     *
     * @return void
     */
    public function test_authCanNotDeleteForeignUser()
    {
        $user = User::factory()->create();

        $foreignUser = User::factory()->create();
        
        Passport::actingAs($user);

        $response = $this->delete('/api/users/'.$foreignUser->id);

        $response->assertForbidden();
    }

    /**
     * Test auth delete himself - 204
     *
     * @return void
     */
    public function test_authCanDeleteHimself()
    {
        $user = User::factory()->create();

        Ad::factory()->state(['user_id' => $user->id])->create();

        Passport::actingAs($user);

        $response = $this->delete('/api/users/'.$user->id);

        $user = User::find($user->id);

        $response->assertNoContent();

        $this->assertEquals(0, Ad::count());

        $this->assertNull($user);
    }
}
