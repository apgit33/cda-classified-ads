<?php

namespace Tests\Api\Ad;

use Laravel\Passport\Passport;
use Tests\TestCase;

use App\Models\Ad\Ad;
use App\Models\Ad\Category;
use App\Models\User\User;

use Illuminate\Http\UploadedFile as UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Tests in this class :
 * ✓ auth can create ad and other i d
 * ✓ auth can not create ad for other
 * ✓ unauth can not create
 * ✓ auth can not create wrong parameters
 * ✓ unauth can access ads
 * ✓ unauth can not access ads wrong parameters
 * ✓ unauth can access ads with limit
 * ✓ unauth can access ads with search
 * ✓ unauth can not access ad empty
 * ✓ unauth can access ad
 * ✓ admin can update others ads and check category
 * ✓ admin can not update unknow ad
 * ✓ auth can not update ad wrong parameters
 * ✓ unauth can not update
 * ✓ auth can not update wrong ad
 * ✓ auth can update his ad
 * ✓ auth can not update other ads
 * ✓ admin can delete other ads
 * ✓ auth can not delete other ads
 * ✓ auth can delete his ads
 * ✓ unauth can not delete
 * ✓ auth can not delete unknow ad
 * ✓ auth can not delete wrong ad
 */
class AdTest extends TestCase
{
    use RefreshDatabase;

    /**
     * ====================
     * ==> TEST POST
     * ====================
     */

    /**
     * Test auth create ad - 201
     *
     * @return void
     */
    public function test_authCanCreateAdAndOtherID()
    {
        Storage::fake('local');

        Category::factory()->count(10)->create();

        $categoryIDs = [];

        foreach (Category::all() as $category) {
            $categoryIDs[] = $category->id;
        }

        $user = User::factory()->create();

        $foreignUser = User::factory()->create();

        Passport::actingAs($user);

        $response = $this->post('/api/ads', [
            'title' => 'my_title',
            'price' => 10,
            'description' => 'test',
            'city' => 'test',
            'user_id' => $foreignUser->id,
            'picture' => UploadedFile::fake()->image('default.png'),
            'categories' => [$categoryIDs[array_rand($categoryIDs)]],
            'is_viewable' => false,
        ]);

        Storage::disk('public')->assertExists('ads/default.png');

        $ad = Ad::first();

        $this->assertEquals($ad->user_id, $user->id);
        
        $response->assertCreated();
    }

    /**
     * Test auth create ad - 201
     *
     * @return void
     */
    public function test_authCanNotCreateAdForOther()
    {
        Category::factory()->count(10)->create();

        $categoryIDs = [];

        foreach (Category::all() as $category) {
            $categoryIDs[] = $category->id;
        }

        $user = User::factory()->create();
        $foreignUser = User::factory()->create();

        Passport::actingAs($user);

        $response = $this->post('/api/ads', [
            'title' => 'my_title',
            'price' => 10,
            'description' => 'test',
            'city' => 'test',
            'user_id' => $foreignUser->id,
            'categories' => [$categoryIDs[array_rand($categoryIDs)]],
        ]);

        $ad = Ad::first();
        
        $this->assertEquals($ad->user_id, $user->id);
        
        $response->assertCreated();
    }

    /**
     * Test unauth create ad - 401
     *
     * @return void
     */
    public function test_unauthCanNotCreate()
    {
        $response = $this->post('/api/ads', [
            //
        ]);

        $response->assertUnauthorized();
    }

    /**
     * Test auth create ad wrong parameter - 422
     *
     * @return void
     */
    public function test_authCanNotCreateWrongParameters()
    {
        Passport::actingAs(User::factory()->create());

        $response = $this->post('/api/ads', [
            'name' => 'test',
        ]);

        $response->assertStatus(422);
    }

    /**
     * ====================
     * ==> TEST GET all
     * ====================
     */

    /**
     * Test unauth access all ads - 200
     *
     * @return void
     */
    public function test_unauthCanAccessAds()
    {
        $response = $this->get('/api/ads');

        $response->assertOk();
    }

    /**
     * Test unauth access all ads wrong parameter- 422
     *
     * @return void
     */
    public function test_unauthCanNotAccessAdsWrongParameters()
    {
        $response = $this->get('/api/ads?category=°°°');

        $response->assertStatus(422);
    }

    /**
     * Test unauth access limit ads- 200
     *
     * @return void
     */
    public function test_unauthCanAccessAdsWithLimit()
    {
        Ad::factory()->count(10)->create();

        $response = $this->get('/api/ads?limit=5');

        $this->assertCount(5, json_decode($response->baseResponse->original));

        $response->assertStatus(200);
    }

    /**
     * Test unauth access search ads- 200
     *
     * @return void
     */
    public function test_unauthCanAccessAdsWithSearch()
    {
        Ad::factory()->count(10)->create();

        $response = $this->get('/api/ads?search=title');

        $this->assertCount(0, json_decode($response->baseResponse->original)->ads);

        $response->assertStatus(200);
    }

    /**
     * ====================
     * ==> TEST GET {id}
     * ====================
     */

    /**
     * Test unauth access unknow ad - 404
     *
     * @return void
     */
    public function test_unauthCanNotAccessAdEmpty()
    {
        Ad::factory()->create();

        $response = $this->get('/api/ads/test');

        $response->assertNotFound();
    }

    /**
     * Test unauth access ad - 200
     *
     * @return void
     */
    public function test_unauthCanAccessAd()
    {
        $ad = Ad::factory()->create();

        $response = $this->get('/api/ads/'.$ad->id);

        $response->assertOk();
    }

    /**
     * ====================
     * ==> TEST PUT
     * ====================
     */

    /**
     * Test admin update other's ads - 200
     *
     * @return void
     */
    public function test_adminCanUpdateOthersAdsAndCheckCategory()
    {
        $category = Category::factory()->state(['name' => 'cat1'])->create();

        Passport::actingAs(User::factory()->admin()->create());

        $user = User::factory()->create();

        $ad = Ad::factory()->state(['user_id' => $user->id])->create();

        $response = $this->put('/api/ads/'.$ad->id, [
            'title' => 'new_name',
            'categories' => [$category->id],
        ]);

        $ad = Ad::with('categories')->findOrFail($ad->id);

        $this->assertEquals($category->id, $ad->categories[0]->id);

        $response->assertOk();

        $this->assertSame('new_name', $ad->title);
    }

    /**
     * Test admin update unknow ad - 404
     *
     * @return void
     */
    public function test_adminCanNotUpdateUnknowAd()
    {
        Passport::actingAs(User::factory()->admin()->create());

        $response = $this->put('/api/ads/1', [
            'title' => 'new_name',
        ]);

        $response->assertNotFound();
    }

    /**
     * Test auth update ad wrong - 422
     *
     * @return void
     */
    public function test_authCanNotUpdateAdWrongParameters()
    {
        Passport::actingAs(User::factory()->create());

        $ad = Ad::factory()->create();

        $response = $this->put('/api/ads/'.$ad->id, [
            'price' => '°°°',
        ]);

        $response->assertStatus(422);
    }

    /**
     * Test unauth update ad - 401
     *
     * @return void
     */
    public function test_unauthCanNotUpdate()
    {
        $response = $this->put('/api/ads/1', [
            'title' => 'new_name',
        ]);

        $response->assertUnauthorized();
    }

    /**
     * Test auth update wrong ad - 500
     *
     * @return void
     */
    public function test_authCanNotUpdateWrongAd()
    {
        Passport::actingAs(User::factory()->create());

        $response = $this->put('/api/ads/°°°', [
            'name' => 'new_name',
        ]);

        $response->assertStatus(500);
    }

    /**
     * Test auth update his ad - 200
     *
     * @return void
     */
    public function test_authCanUpdateHisAd()
    {
        Passport::actingAs(User::factory()->create());

        $ad = Ad::factory()->create();

        $response = $this->put('/api/ads/'.$ad->id, [
            'title' => 'new_name',
        ]);

        $ad = Ad::findOrFail($ad->id);

        $response->assertOk();
        
        $this->assertSame('new_name', $ad->title);
    }

    /**
     * Test auth update other ad - 403
     *
     * @return void
     */
    public function test_authCanNotUpdateOtherAds()
    {
        $user = User::factory()->create();
        
        $foreignUser = User::factory()->create();

        Passport::actingAs($user);

        Ad::factory()->state(['user_id' => $user->id])->create();
        
        $ad2 = Ad::factory()->state(['user_id' => $foreignUser->id])->create();

        $response = $this->put('/api/ads/'.$ad2->id, [
            'title' => 'new_name',
        ]);

        $response->assertForbidden();
    }

    /**
     * ====================
     * ==> TEST DELETE
     * ====================
     */

    /**
     * Test admin delete other ads - 204
     *
     * @return void
     */
    public function test_adminCanDeleteOtherAds()
    {
        Passport::actingAs(User::factory()->admin()->create());

        $user = User::factory()->create();

        $ad = Ad::factory()->state(['user_id' => $user->id])->create();

        $response = $this->delete('/api/ads/'.$ad->id);

        $response->assertNoContent();

        $this->assertNull(Ad::find($ad->id));
    }

    /**
     * Test auth delete other ads - 403
     *
     * @return void
     */
    public function test_authCanNotDeleteOtherAds()
    {
        Passport::actingAs(User::factory()->create());

        $user = User::factory()->create();

        $ad = Ad::factory()->state(['user_id' => $user->id])->create();

        $response = $this->delete('/api/ads/'.$ad->id);

        $response->assertForbidden();

        $this->assertNotNull(Ad::find($ad->id));
    }

    /**
     * Test auth delete his ads - 204
     *
     * @return void
     */
    public function test_authCanDeleteHisAds()
    {
        Passport::actingAs(User::factory()->create());

        $ad = Ad::factory()->create();

        $response = $this->delete('/api/ads/'.$ad->id);

        $response->assertNoContent();

        $this->assertNull(Ad::find($ad->id));
    }

    /**
     * Test unauth delete ad - 401
     *
     * @return void
     */
    public function test_unauthCanNotDelete()
    {
        $response = $this->delete('/api/ads/1');

        $response->assertUnauthorized();
    }

    /**
     * Test auth delete unknow ad - 404
     *
     * @return void
     */
    public function test_authCanNotDeleteUnknowAd()
    {
        Passport::actingAs(User::factory()->create());

        $response = $this->delete('/api/ads/1');

        $response->assertNotFound();
    }

    /**
     * Test auth delete wrong ad - 500
     *
     * @return void
     */
    public function test_authCanNotDeleteWrongAd()
    {
        Passport::actingAs(User::factory()->create());

        $response = $this->delete('/api/ads/test');

        $response->assertStatus(500);
    }
}
