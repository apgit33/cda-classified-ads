<?php

namespace Tests\Api\Ad;

use Laravel\Passport\Passport;
use Tests\TestCase;

use App\Models\Ad\Category;
use App\Models\User\User;

use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Tests in this class :
 * ✓ admin can create
 * ✓ auth can not create
 * ✓ admin can not create existing
 * ✓ admin can not create wrong parameters
 * ✓ unauth can access categories
 * ✓ unauth can not access categories wrong parameters
 * ✓ unauth can not access category empty
 * ✓ unauth can access category
 * ✓ admin can update
 * ✓ admin can not update unknow category
 * ✓ admin can not update wrong parameters
 * ✓ unauth can not update
 * ✓ admin can not update wrong category
 * ✓ admin can delete
 * ✓ unauth can not delete
 * ✓ admin can not delete unknow
 * ✓ admin can not delete wrong
 */
class CategoryTest extends TestCase
{
    use RefreshDatabase;

    /**
     * ====================
     * ==> TEST POST
     * ====================
     */

    /**
     * Test admin create categories - 201
     *
     * @return void
     */
    public function test_adminCanCreate()
    {
        Passport::actingAs(User::factory()->admin()->create());

        $response = $this->post('/api/ads/categories', [
            'name' => 'test',
            'icon_name' => 'test'
        ]);

        $response->assertCreated();
    }

    /**
     * Test auth create categories - 401
     *
     * @return void
     */
    public function test_authCanNotCreate()
    {
        Passport::actingAs(User::factory()->create());
        
        $response = $this->post('/api/ads/categories', [
            'name' => 'test',
            'icon_name' => 'test'
        ]);

        $response->assertUnauthorized();
    }

    /**
     * Test admin create same categories - 422
     *
     * @return void
     */
    public function test_adminCanNotCreateExisting()
    {
        Passport::actingAs(User::factory()->admin()->create());

        $category = Category::factory()->create();

        $response = $this->post('/api/ads/categories', [
            'name' => $category->name,
        ]);

        $response->assertStatus(422);
    }

    /**
     * Test admin create categories wrong parameter - 422
     *
     * @return void
     */
    public function test_adminCanNotCreateWrongParameters()
    {
        Passport::actingAs(User::factory()->admin()->create());

        $response = $this->post('/api/ads/categories', [
            'name' => 'test',
        ]);

        $response->assertStatus(422);
    }

    /**
     * ====================
     * ==> TEST GET all
     * ====================
     */

    /**
     * Test unauth access all categories - 200
     *
     * @return void
     */
    public function test_unauthCanAccessCategories()
    {
        Category::factory(10)->create();

        $response = $this->get('/api/ads/categories');

        $response->assertOk();
    }

    /**
     * Test unauth access all categories wrong - 422
     *
     * @return void
     */
    public function test_unauthCanNotAccessCategoriesWrongParameters()
    {
        $response = $this->get('/api/ads/categories?name=°°°');

        $response->assertStatus(422);
    }

    /**
     * ====================
     * ==> TEST GET {id}
     * ====================
     */

    /**
     * Test unauth access unknow category - 404
     *
     * @return void
     */
    public function test_unauthCanNotAccessCategoryEmpty()
    {
        Category::factory()->create();

        $response = $this->get('/api/ads/categories/test');

        $response->assertNotFound();
    }

    /**
     * Test unauth access category - 200
     *
     * @return void
     */
    public function test_unauthCanAccessCategory()
    {
        $category = Category::factory()->create();

        $response = $this->get('/api/ads/categories/'.$category->id);

        $response->assertOk();
    }

    /**
     * ====================
     * ==> TEST PUT
     * ====================
     */

    /**
     * Test admin update categories - 200
     *
     * @return void
     */
    public function test_adminCanUpdate()
    {
        Passport::actingAs(User::factory()->admin()->create());

        $category = Category::factory()->create();

        $response = $this->put('/api/ads/categories/'.$category->id, [
            'name' => 'new_name',
        ]);
        $category = Category::findOrFail($category->id);

        $response->assertOk();
        $this->assertSame('new_name', $category->name);
    }

    /**
     * Test admin update unknow category - 404
     *
     * @return void
     */
    public function test_adminCanNotUpdateUnknowCategory()
    {
        Passport::actingAs(User::factory()->admin()->create());

        $response = $this->put('/api/ads/categories/1', [
            'name' => 'new_name',
        ]);

        $response->assertNotFound();
    }

    /**
     * Test admin update categories wrong - 422
     *
     * @return void
     */
    public function test_adminCanNotUpdateWrongParameters()
    {
        Passport::actingAs(User::factory()->admin()->create());

        $category = Category::factory()->create();

        $response = $this->put('/api/ads/categories/'.$category->id, [
            'name' => '°°°',
        ]);

        $response->assertStatus(422);
    }

    /**
     * Test unauth update categories - 401
     *
     * @return void
     */
    public function test_unauthCanNotUpdate()
    {
        $response = $this->put('/api/ads/categories/1', [
            'name' => 'new_name',
        ]);

        $response->assertUnauthorized();
    }

    /**
     * Test admin update wrong category - 500
     *
     * @return void
     */
    public function test_adminCanNotUpdateWrongCategory()
    {
        Passport::actingAs(User::factory()->admin()->create());

        $response = $this->put('/api/ads/categories/°°°', [
            'name' => 'new_name',
        ]);

        $response->assertStatus(500);
    }

    /**
     * ====================
     * ==> TEST DELETE
     * ====================
     */

    /**
     * Test admin delete categories - 204
     *
     * @return void
     */
    public function test_adminCanDelete()
    {
        Passport::actingAs(User::factory()->admin()->create());

        $category = Category::factory()->create();

        $response = $this->delete('/api/ads/categories/'.$category->id, );

        $category = Category::find($category->id);

        $response->assertNoContent();
        $this->assertNull($category);
    }

    /**
     * Test unauth delete categories - 401
     *
     * @return void
     */
    public function test_unauthCanNotDelete()
    {
        $response = $this->delete('/api/ads/categories/1');

        $response->assertUnauthorized();
    }

    /**
     * Test admin delete unknow category - 404
     *
     * @return void
     */
    public function test_adminCanNotDeleteUnknow()
    {
        Passport::actingAs(User::factory()->admin()->create());

        $response = $this->delete('/api/ads/categories/1');

        $response->assertNotFound();
    }

    /**
     * Test admin delete wrong category - 500
     *
     * @return void
     */
    public function test_adminCanNotDeleteWrong()
    {
        Passport::actingAs(User::factory()->admin()->create());

        $response = $this->delete('/api/ads/categories/test');

        $response->assertStatus(500);
    }
}
