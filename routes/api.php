<?php

use App\Http\Controllers\Api\Ad\CategoryController;
use App\Http\Controllers\Api\Ad\AdController;
use App\Http\Controllers\Api\User\UserController;
use App\Http\Controllers\Auth\AuthController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'namespace'  => 'App\Http\Controllers\Api',
], function () { // custom admin routes
    Route::group([ // auth route
        'middleware' => ['auth:api'],
    ], function () {
        Route::group([
            'namespace'  => 'Auth',
        ], function () {
            Route::post('/logout', [AuthController::class, 'logout']);
        });
    
        Route::group([
            'prefix' => '/users',
            'namespace'  => 'User',
        ], function () {
            Route::put('/{user}', [UserController::class, 'update']);
            Route::delete('/{user}', [UserController::class, 'delete']);
        });
    
        Route::group([
            'prefix' => '/ads',
            'namespace'  => 'Ad',
        ], function () {
            Route::group([
                'prefix' => '/categories',
                'middleware' => 'api.admin',
            ], function () {
                Route::post('/', [CategoryController::class, 'store']);
                Route::put('/{category}', [CategoryController::class, 'update']);
                Route::delete('/{category}', [CategoryController::class, 'delete']);
            });
    
            Route::post('/', [AdController::class, 'store']);
            Route::put('/{ad}', [AdController::class, 'update']);
            Route::delete('/{ad}', [AdController::class, 'delete']);
        });
    });
    
    Route::group([ // unauth route
        //
    ], function () {
        Route::group([
            'namespace'  => 'Auth',
        ], function () {
            Route::post('/forgot-password', [AuthController::class, 'forgotPassword'])->name('passwords.sent');
            Route::post('/reset-password/{token}', [AuthController::class, 'resetPassword'])->name('password.reset');
        });
    
        Route::group([
            'prefix' => 'users',
            'namespace'  => 'User',
        ], function () {
            Route::get('/', [UserController::class, 'index']);
            Route::post('/', [UserController::class, 'store']);
            Route::get('/{user}', [UserController::class, 'show']);
        });
    
        Route::group([
            'prefix' => 'ads',
            'namespace'  => 'Ad',
        ], function () {
            Route::group([
                'prefix' => 'categories'
            ], function () {
                Route::get('/', [CategoryController::class, 'index']);
                Route::get('/{category}', [CategoryController::class, 'show']);
            });
    
            Route::get('/', [AdController::class, 'index'])->name('home');
            Route::get('/{ad}', [AdController::class, 'show']);
        }); 
    });
});
