<?php

namespace App\Policies\Ad;

use App\Models\Ad\Ad;
use App\Models\User\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class AdPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User\User  $user
     * @param  \App\Models\Ad\Ad  $ad
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Ad $ad)
    {
        return ($user->is_admin || $user->ads->contains($ad->id));
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User\User  $user
     * @param  \App\Models\Ad\Ad  $ad
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Ad $ad)
    {
        return ($user->is_admin || $user->ads->contains($ad->id));
    }
}
