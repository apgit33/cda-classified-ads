<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class GenerateOA extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'OA:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate commentary for Open API';

    /**
     * Models de l'API
     *
     * @var array
     */
    protected $tables = [
        'User',
        'Ad\Ad',
        'Ad\Category',
    ];

    /**
     * Les informations retenues
     * 
     * @var array
     */
    protected $datas=[];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $db = DB::getSchemaBuilder();
        foreach ($this->tables as $table) {
            $table = "App\Models\\$table";
            $table = (new $table())->getTable(); // on récupère la table
            $columns = $db->getColumnListing($table); // on liste les colonnes de la table
            foreach ($columns as $column) {
                if (!in_array($column, ["created_at","updated_at","deleted_at"])) { // on retire les champs non voulus
                    $type = $db->getColumnType($table, $column); // on récupère le type de la colonne
                    $ex=$type;
                    if ($type != "integer" && $type != "bigint") { // on vérifie différents types de valeur pour créer une valeur d'exemple
                        if ($type != "boolean") {
                            if ($type == "datetime") {
                                $ex = date("Y-m-d H:i:s", mt_rand(1262055681, 1262055681));
                            }
                        } else {
                            $ex = true;
                        }
                    } else {
                        $ex = random_int(1, 10);
                    }

                    $this->datas[$table][] = [ // on construit le tableau 
                        'property' => $column,
                        'format' => $type,
                        'title' => $column,
                        'example' => $ex
                    ];
                }
            }
        }
        $body = view('api.doc_generator', [ // on envoie le tableau généré à la vue
            'schemas' => $this->datas,
        ])->render();

        file_put_contents('app/Http/Controllers/Api/Schema.php', "<?php\n".$body); // on construit le fichier php
    
    }
}
