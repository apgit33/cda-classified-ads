<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

use App\Models\User\User;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * @OA\Post(
     *      path="/forgot-password",
     *      operationId="api.forgot-password",
     *      tags={"Auth"},
     *      summary="Retrieve password",
     *      description="Retrieve password",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"email"},
     *              @OA\Property(type="string",property="email"),
     *          ),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Email send",
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity"
     *      ),
     * )
     *
     * Retrieve password
     *
     * @param Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function forgotPassword(Request $request)
    {
        $request->validate(['email' => 'required|email']);

        Password::sendResetLink($request->only('email'));
    
        return response(['success' => 'Email send'], 200);
    }

    /**
     * @OA\Post(
     *      path="/reset-password/{token}",
     *      operationId="api.reset-password",
     *      tags={"Auth"},
     *      summary="Reset password",
     *      description="Reset password",
     *      @OA\Parameter(
     *          name="token",
     *          description="Token reset password",
     *          required=true,
     *          @OA\Schema(type="string"),
     *          in="path"
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"email","password"},
     *              @OA\Property(type="string",property="email"),
     *              @OA\Property(type="string",property="password"),
     *          ),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Password changed or Token expired",
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity"
     *      ),
     * )
     *
     * Retrieve password
     *
     * @param Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function resetPassword(string $token, Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $user = Password::getUser(['email' => $request->email]);

        if (Password::tokenExists($user, $token)) {
            User::whereEmail($request->email)->first()->update([
                'password' => Hash::make($request['password']),
            ]);

            Password::deleteToken($user);

            return response('Password changed', 200);
        }

        return response(['error' => 'Token expired'], 200);
    }

    /**
     * @OA\Post(
     *      path="/logout",
     *      operationId="api.logout",
     *      tags={"Auth"},
     *      summary="Logout",
     *      description="Logout",
     *      @OA\Response(
     *          response=200,
     *          description="Logout",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     * )
     *
     * Logout
     *
     * @param Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    { // TODO check inutile ?
        if (Auth::check()) {
            if (!Auth::user()->token()->revoke()) {
                $token = Auth::user()->tokens;
                
                $token[0]->update([
                    'revoked' => true
                ]);
            }
        }

        $response = ['message' => 'You have been successfully logged out!'];
        
        return response($response, 200);
    }
}
