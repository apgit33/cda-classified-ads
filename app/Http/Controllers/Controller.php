<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Api Documentation",
     *      description="Implementation of Swagger with in Laravel",
     *      @OA\Contact(
     *          email="admin@admin.com"
     *      ),
     *      @OA\License(
     *          name="Apache 2.0",
     *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *      )
     * )
     * @OA\SecurityScheme(
     *      securityScheme="bearerAuth",
     *      in="header",
     *      name="bearerAuth",
     *      type="http",
     *      scheme="bearer",
     * ),
     * @OA\Security(
     *      name="bearerAuth",
     * ),
     * 
     * @OA\Server(
     *      url=L5_SWAGGER_CONST_HOST,
     *      description="Demo API Server"
     * )
     * 
     * @OA\Tag(
     *     name="Categories",
     *     description="API Endpoints of Categories",
     * )
     * 
     * @OA\Tag(
     *     name="Ads",
     *     description="API Endpoints of Ads",
     * )
     * 
     * @OA\Tag(
     *     name="Users",
     *     description="API Endpoints of Users",
     * )
     * 
     */
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

}
