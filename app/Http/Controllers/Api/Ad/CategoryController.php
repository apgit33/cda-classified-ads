<?php

namespace App\Http\Controllers\Api\Ad;

use App\Http\Controllers\Controller;
use App\Http\Resources\Ad\CategoryCollection;
use App\Http\Resources\Ad\CategoryResource;

use App\Models\Ad\Category;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CategoryController extends Controller
{
    /**
     * @OA\Get(
     *      path="/ads/categories",
     *      operationId="api.ads.category",
     *      tags={"Categories"},
     *      summary="Get list of categories of ads",
     *      description="Returns all categories of ads",
     *      @OA\Parameter(
     *          name="limit",
     *          description="Limit number of results",
     *          required=false,
     *          @OA\Schema(type="integer"),
     *          in="query",
     *      ),
     *      @OA\Parameter(
     *          name="name",
     *          description="Get category by name",
     *          required=false,
     *          @OA\Schema(type="string", format="alpha_num_dash"),
     *          in="query",
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="List of categories",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Categories")),
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity"
     *      ),
     *  )
     *
     * Get list of categories
     *
     * @param Illuminate\Http\Request $request
     *
     * @return CategoryCollection|CategoryResource
     */
    public function index(Request $request)
    {
        $request->validate([
            'limit' => 'sometimes|numeric',
            'name' => 'sometimes|alpha_dash'
        ]);

        $limit = $request->input('limit', 100); //limit base
        $name = $request->input('name'); // name

        $categories = Category::take($limit)
            ->when(!empty($name), function ($query) use ($name) {
                return $query->whereName($name);
            })
            ->get();

        if ($categories->count() > 1) {
            return (new CategoryCollection($categories))
                ->additional([
                    'limit' => $limit,
            ]);
        }

        return new CategoryResource($categories);
    }

    /**
     *
     * @OA\Post(
     *      path="/ads/categories",
     *      operationId="api.ads.category.store",
     *      tags={"Categories"},
     *      summary="Store a newly created category",
     *      description="Store a category in storage",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"name","icon_name"},
     *              @OA\Property(type="string",property="name"),
     *              @OA\Property(type="string",property="icon_name"),
     *          ),
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Infos of category",
     *          @OA\JsonContent(ref="#/components/schemas/Categories"),
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity"
     *      ),
     * )
     *
     * Store a newly created resource in storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return CategoryResource
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|alpha_dash|unique:categories',
            'icon_name' => 'required|string',
        ]);

        $dataToInsert = $request->except(null);

        $category = Category::create($dataToInsert);

        return new CategoryResource($category);
    }

    /**
     * @OA\Get(
     *      path="/ads/categories/{id}",
     *      operationId="api.categories.show",
     *      tags={"Categories"},
     *      summary="Informations  entrant",
     *      description="Retourne les informations détaillées sur un contact entrant",
     *      @OA\Parameter(
     *          name="id",
     *          description="Identifiant du contact",
     *          required=true,
     *          @OA\Schema(type="integer"),
     *          in="path"
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Information sur le contact",
     *          @OA\JsonContent(ref="#/components/schemas/Categories"),
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Not found"
     *      ),
     * )
     *
     * Display the specified resource.
     *
     * @param  integer $id
     *
     * @return CategoryResource
     */
    public function show($id)
    {
        return new CategoryResource(Category::findOrFail($id));
    }

    /**
     * @OA\Delete(
     *      path="/ads/categories/{id}",
     *      operationId="api.ads.category.delete",
     *      tags={"Categories"},
     *      summary="Delete cat",
     *      description="Delete cat",
     *      @OA\Parameter(
     *          name="id",
     *          description="Cat",
     *          required=true,
     *          @OA\Schema(type="integer"),
     *          in="path",
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="No content"
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not found"
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal Error"
     *      ),
     * )
     *
     * Remove the specified resource from storage.
     *
     * @param  integer  $id
     *
     * @return noContent
     */
    public function delete(int $id)
    {
        Category::findOrFail($id)->delete();

        return response()->noContent();
    }

    /**
     * @OA\Put(
     *      path="/ads/categories/id",
     *      operationId="api.ads.category.update",
     *      tags={"Categories"},
     *      summary="Update a newly created category",
     *      description="Update a category in storage",
     *      @OA\Parameter(
     *          name="id",
     *          description="Identifiant du contact",
     *          required=true,
     *          @OA\Schema(type="integer"),
     *          in="path"
     *      ),
     *      @OA\RequestBody(
     *           @OA\JsonContent(ref="#/components/schemas/Categories"),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Infos of category",
     *          @OA\JsonContent(ref="#/components/schemas/Categories"),
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not found"
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity"
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal Error"
     *      ),
     * )
     *
     * Update a resource in storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return CategoryResource
     */
    public function update(int $id, Request $request)
    {
        $request->validate([
            'name' => [
                'sometimes',
                'required',
                'alpha_dash',
                Rule::unique('categories')->ignore($id),
            ],
            'icon_name' => 'sometimes|required|string',
        ]);

        $category = Category::findOrFail($id);

        $category->update($request->only(['name','icon_name']));

        return new CategoryResource($category);
    }
}
