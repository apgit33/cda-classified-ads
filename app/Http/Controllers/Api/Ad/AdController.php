<?php

namespace App\Http\Controllers\Api\Ad;

use App\Http\Controllers\Controller;
use App\Http\Resources\Ad\AdCollection;
use App\Http\Resources\Ad\AdResource;

use App\Models\Ad\Ad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdController extends Controller
{
    /**
     * @OA\Get(
     *      path="/ads",
     *      operationId="api.ads",
     *      tags={"Ads"},
     *      summary="Get list of ads",
     *      description="Returns all ads",
     *      @OA\Parameter(
     *          name="limit",
     *          description="Limit number of results",
     *          required=false,
     *          @OA\Schema(type="integer"),
     *          in="query",
     *      ),
     *      @OA\Parameter(
     *          name="search",
     *          description="Get ads by search",
     *          required=false,
     *          @OA\Schema(type="string"),
     *          in="query",
     *      ),
     *      @OA\Parameter(
     *          name="category",
     *          description="Get ads by category",
     *          required=false,
     *          @OA\Schema(type="string"),
     *          in="query",
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="List of ads",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Ads")),
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity"
     *      )
     *  )
     *
     * Get list of ads
     *
     * @param Illuminate\Http\Request $request
     *
     * @return AdCollection|AdResource
     */
    public function index(Request $request)
    {
        $request->validate([
            'limit' => 'sometimes|numeric',
            'category' => 'sometimes|alpha_dash',
        ]);

        $limit = $request->input('limit', 100); //limit base
        $category = $request->input('category'); // category
        $search = $request->input('search'); // search

        $ads = Ad::take($limit)
            ->when(!empty($category), function ($query) use ($category) {
                return $query->whereHas('categories', function ($query) use ($category) {
                    $query->where('name', 'LIKE', '%'.$category.'%');
                });
            })
            ->when(!empty($search), function ($query) use ($search) {
                return $query->where('title', 'LIKE', '%'.$search.'%');
            })
            ->get();

        if ($ads->count() > 1) {
            return (new AdCollection($ads))
                ->additional([
                    'limit' => $limit,
            ]);
        }

        if ($ads->count() == 0) return json_encode(['ads' => []]);

        return new AdResource($ads);
    }

    /**
     *
     * @OA\Post(
     *      path="/ads",
     *      operationId="api.ads.store",
     *      tags={"Ads"},
     *      summary="Store a newly created ad",
     *      description="Store a ad in storage",
     *      @OA\RequestBody(
     *         description="Pet to add to the store",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(ref="#/components/schemas/Ads")
     *         )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Infos of ad",
     *          @OA\JsonContent(ref="#/components/schemas/Ads"),
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity"
     *      )
     * )
     *
     * Store a newly created resource in storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return AdResource
     */
    public function store(Request $request)
    {
        $userID = Auth::user()->id;

        $request->merge([
            'user_id' => $userID,
        ]);

        if ($request->is_formData) {
            $request->merge([
                'categories' => json_decode($request->categories),
                'is_viewable' => filter_var($request->is_viewable, FILTER_VALIDATE_BOOLEAN),
            ]);
        }

        $request->validate([
            'title' => 'required|string',
            'price' => 'required|integer',
            'description' => 'required',
            'city' => 'required',
            'user_id' => 'required|numeric',
            'picture' => 'sometimes|image',
            'categories' => 'required|array',
            'categories.*' => 'exists:categories,id',
            'is_viewable' => 'sometimes|boolean',
        ]);

        $dataToInsert = $request->except(null);

        if ($request->hasFile('picture') && $request->file('picture')->isValid()) {
            $lastID = Ad::latest()->first()->id ?? 1;
        
            $file = $request->file('picture')->storePubliclyAs('ads/'.$lastID, $request->file('picture')->getClientOriginalName(), 'public');
            
            $dataToInsert['picture'] = $file;
        }

        $ad = Ad::create($dataToInsert);

        $ad->categories()->sync($dataToInsert['categories']);

        return new AdResource($ad);
    }

    /**
     * @OA\Get(
     *      path="/ads/{id}",
     *      operationId="api.ads.show",
     *      tags={"Ads"},
     *      summary="Informations  entrant",
     *      description="Retourne les informations détaillées sur un contact entrant",
     *      @OA\Parameter(
     *          name="id",
     *          description="Identifiant du contact",
     *          required=true,
     *          @OA\Schema(type="integer"),
     *          in="path"
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Information sur le contact",
     *          @OA\JsonContent(ref="#/components/schemas/Ads"),
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Not found"
     *      )
     * )
     *
     * Display the specified resource.
     *
     * @param  integer $id
     *
     * @return AdResource
     */
    public function show($id)
    {
        return new AdResource(Ad::with('categories')->findOrFail($id));
    }

    /**
     * @OA\Delete(
     *      path="/ads/{id}",
     *      operationId="api.ads.delete",
     *      tags={"Ads"},
     *      summary="Delete ad",
     *      description="Delete ad",
     *      @OA\Parameter(
     *          name="id",
     *          description="Cat",
     *          required=true,
     *          @OA\Schema(type="integer"),
     *          in="path",
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="No content"
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not found"
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal Error"
     *      ),
     * )
     *
     * Remove the specified resource from storage.
     *
     * @param  integer  $id
     * @param Illuminate\Http\Request $request
     *
     * @return noContent
     */
    public function delete(int $id, Request $request)
    {
        if ($request->user()->cannot('delete', Ad::findOrFail($id))) {
            abort(403);
        }

        Ad::findOrFail($id)->delete();

        return response()->noContent();
    }

    /**
     *
     * @OA\Put(
     *      path="/ads",
     *      operationId="api.ads.update",
     *      tags={"Ads"},
     *      summary="Update a ad",
     *      description="Update a ad in storage",
     *      @OA\RequestBody(
     *           @OA\JsonContent(ref="#/components/schemas/Ads"),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Infos of ad",
     *          @OA\JsonContent(ref="#/components/schemas/Ads"),
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not found"
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity"
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal Error"
     *      ),
     * )
     *
     * Update a resource in storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return AdResource
     */
    public function update(int $id, Request $request)
    {
        if ($request->user()->cannot('update', Ad::findOrFail($id))) {
            abort(403);
        }

        $request->validate([
            'title' => 'sometimes|string',
            'price' => 'sometimes|numeric',
            'description' => 'sometimes',
            'city' => 'sometimes',
            'picture' => 'sometimes|image',
            'categories' => 'sometimes|array',
            'categories.*' => 'required_with:categories|exists:categories,id',
            'is_viewable' => 'sometimes|boolean'
        ]);

        $ad = Ad::with('categories')->findOrFail($id);

        $ad->categories()->sync($request->categories);

        if (Auth::user()->is_admin) {
            $ad->update($request->all());
        } else {
            $ad->update($request->only(['title','price','description','city','picture','is_viewable']));
        }
        return new AdResource($ad);
    }
}
