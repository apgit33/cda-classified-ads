<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\UserCollection;
use App\Http\Resources\User\UserResource;

use App\Models\User\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * @OA\Post(
     *      path="/users",
     *      operationId="api.users.store",
     *      tags={"Users"},
     *      summary="Store a newly created user",
     *      description="Store a user in storage",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"name","email","password","password_confirmation"},
     *              @OA\Property(type="string",property="name"),
     *              @OA\Property(type="string",property="email"),
     *              @OA\Property(type="string",property="password"),
     *              @OA\Property(type="string",property="password_confirmation"),
     *          ),
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Infos of user",
     *          @OA\JsonContent(ref="#/components/schemas/Users"),
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity"
     *      ),
     * )
     *
     * Store a newly created resource in storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return UserResource
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|alpha_dash|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
        ]);

        $request['password'] = Hash::make($request->input('password'));

        $request['remember_token'] = Str::random(10);

        $dataToInsert = $request->only(['name','email','password']);

        $user = User::create($dataToInsert);

        return new UserResource($user);
    }

    /**
     * @OA\Get(
     *      path="/users",
     *      operationId="api.users",
     *      tags={"Users"},
     *      summary="Get list of users",
     *      description="Returns all users",
     *      @OA\Parameter(
     *          name="limit",
     *          description="Limit number of results",
     *          required=false,
     *          @OA\Schema(type="integer"),
     *          in="query",
     *      ),
     *      @OA\Parameter(
     *          name="name",
     *          description="Get users by name",
     *          required=false,
     *          @OA\Schema(type="string"),
     *          in="query",
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="List of users",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Users")),
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity"
     *      ),
     * )
     *
     * Get list of users
     *
     * @param Illuminate\Http\Request $request
     *
     * @return UserCollection|UserResource
     */
    public function index(Request $request)
    {
        $request->validate([
            'limit' => 'sometimes|numeric',
            'name' => 'sometimes|alpha_num',
            'email' => 'sometimes|email'
        ]);

        $limit = $request->input('limit', 100); //limit base
        $name = $request->input('name'); // name
        $email = $request->input('email'); // email

        $users = User::take($limit)
            ->when(!empty($name), function ($query) use ($name) {
                return $query->whereName($name);
            })
            ->when(!empty($email), function ($query) use ($email) {
                return $query->whereEmail($email);
            })
            ->get();

        if ($users->count() > 1) {
            return (new UserCollection($users))
                ->additional([
                    'limit' => $limit,
            ]);
        } elseif ($users->count() == 0) {
            return json_encode(['users' => []]);
        }

        return new UserResource($users);
    }

    /**
     * @OA\Get(
     *      path="/users/{id}",
     *      operationId="api.users.show",
     *      tags={"Users"},
     *      summary="Informations  entrant",
     *      description="Retourne les informations détaillées sur un contact entrant",
     *      @OA\Parameter(
     *          name="id",
     *          description="Identifiant du contact",
     *          required=true,
     *          @OA\Schema(type="integer"),
     *          in="path"
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Information sur le contact",
     *          @OA\JsonContent(ref="#/components/schemas/Users"),
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Not found"
     *      ),
     * )
     *
     * Display the specified resource.
     *
     * @param  integer $id
     *
     * @return UserResource
     */
    public function show($id)
    {
        return new UserResource(User::findOrFail($id));
    }

    /**
     * @OA\Delete(
     *      path="/users/{id}",
     *      operationId="api.user.delete",
     *      tags={"Users"},
     *      summary="Delete user",
     *      description="Delete user",
     *      @OA\Parameter(
     *          name="id",
     *          description="Cat",
     *          required=true,
     *          @OA\Schema(type="integer"),
     *          in="path",
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="No content"
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not found"
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal Error"
     *      ),
     * )
     *
     * Remove the specified resource from storage.
     *
     * @param  integer  $id
     * @param Illuminate\Http\Request $request
     *
     * @return json
     */
    public function delete(int $id, Request $request) // TODO token, AD, image
    {
        if ($request->user()->cannot('delete', User::findOrFail($id))) {
            abort(403);
        }

        User::findOrFail($id)->delete();

        return response()->noContent();
    }

    /**
     * @OA\Put(
     *      path="/users/id",
     *      operationId="api.user.update",
     *      tags={"Users"},
     *      summary="Update a user",
     *      description="Update a user in storage",
     *      @OA\Parameter(
     *          name="id",
     *          description="Identifiant du contact",
     *          required=true,
     *          @OA\Schema(type="integer"),
     *          in="path"
     *      ),
     *      @OA\RequestBody(
     *           @OA\JsonContent(ref="#/components/schemas/Users"),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Infos of user",
     *          @OA\JsonContent(ref="#/components/schemas/Users"),
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not found"
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity"
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal Error"
     *      ),
     * )
     *
     * Update a resource in storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return UserResource
     */
    public function update(int $id, Request $request)
    {
        if ($request->user()->cannot('update', User::findOrFail($id))) {
            abort(403);
        }

        $request->validate([
            'name' => [
                'sometimes',
                'alpha_num',
                Rule::unique('users')->ignore($id)
            ],
            'email' => [
                'sometimes',
                'email',
                Rule::unique('users')->ignore($id)
            ],
            'password' => 'sometimes|confirmed',
            'password_confirmation' => 'required_with:password'
        ]);

        $user = User::findOrFail($id);

        if (Auth::user()->is_admin) {
            $user->update($request->all());
        } else {
            $user->update($request->only(['name','email','password']));
        }

        return new UserResource($user);
    }
}
