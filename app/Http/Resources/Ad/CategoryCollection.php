<?php

namespace App\Http\Resources\Ad;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CategoryCollection extends ResourceCollection
{/**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    // public static $wrap = 'categories';
    
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection;
    }
}
