<?php

namespace App\Http\Resources\Ad;

use Illuminate\Http\Resources\Json\JsonResource;

class AdResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'ad';

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data =  [
            'id' => $this->id,
            'title' => $this->title,
            'price' => $this->price,
            'description' => $this->description,
            'city' => $this->city,
            'user_name' => $this->user->name,
            'picture' => $this->picture,
            'created_at' => $this->created_at->format('d/m/y'),
        ];

        foreach ($this->categories as $category) {
            $data['categories'][$category->id] = [
                'id' => $category->id,
                'name' => $category->name
            ];
        }

        return $data;
    }
}
