<?php

namespace App\Models\Ad;

use App\Models\Ad\Ad;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory, CrudTrait;

    protected $fillable = [
        'name',
        'icon_name',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'pivot',
    ];

    /**
     * ads
     * 
     * @return Relation
     */
    public function ads()
    {
        return $this->belongsToMany(Ad::class);
    }
}
