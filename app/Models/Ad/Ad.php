<?php

namespace App\Models\Ad;

use App\Models\Ad\Category;
use App\Models\User\User;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    use HasFactory, CrudTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'price',
        'description',
        'status',
        'city',
        'status',
        'user_id',
        'picture',
        'is_viewable',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_viewable' => 'boolean'
    ];

    /**
     * user
     * 
     * @return Relation
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * categories
     * 
     * @return Relation
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}
