<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>

<li class="nav-item nav-dropdown">
    <a href="#" class="nav-link nav-dropdown-toggle">
        <i class="nav-icon la la-user"></i>User
    </a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('user/user') }}'><i class='nav-icon la la-user'></i>Users</a></li>
    </ul>
</li>

<li class="nav-item nav-dropdown">
    <a href="#" class="nav-link nav-dropdown-toggle">
        <i class="nav-icon la la-newspaper"></i>Ad
    </a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('ad/ad') }}'><i class='nav-icon la la-question'></i>Ads</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('ad/category') }}'><i class='nav-icon la la-question'></i>Categories</a></li>
    </ul>
</li>