/** 
@foreach ($schemas as $schema => $properties)
* @OA\Schema(
*    schema="{{ ucfirst($schema) }}",
@foreach ($properties as $property)
*    @OA\Property(
*        property="{{ $property['property'] }}",
*        format="{{ $property['format'] }}",
*        title="{{ $property['title'] }}",
*        example="{{ $property['example'] }}",
*   ),
@endforeach
*)
*
@endforeach
*/